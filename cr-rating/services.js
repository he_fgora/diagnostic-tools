
const rp = require('request-promise')
const api = require('./api')
const utils = require('./utils')
const cfg = require('./config')

/**
 * Submit a quote with GW / PE / PC
 * @param {json} payload 
 * @returns 
 */
function callGW(payload) {
    // Accept file with payload inside *-processed.json file. 
    if (payload[0] && payload[0].input) payload = payload[0].input

    const options = {
        uri: `https://pc-qa-e1p-amfam-edge.api.dev-1.us-east-1.guidewire.net/service/edge/pc/transaction`,
        method: 'POST',
        json: true,
        headers: {
            authorization: 'Basic c2Fsc3J2dDpSR2FpeXpEMzRvTVRsdThX'
        },
        body: payload
    }
    return api.restApi(options, 'transfer-response.json', 'PE Service')
}

function submitTransferPayload(file) {
    return utils.readJsonPromise(file)
        .then(data => callGW(data))
        .then(response => console.log(utils.stringify(response)))
        .catch(err => console.error(`Failed to call GW: ${err}`))
}

/**
 * 
 * @param {string} payload : Payload sent to the Rating service 
 * @param {string} lob : string indicating 'auto' or 'property'
 * @returns 
 */
function callRatingService(payload, lob) {
    const index_processed = 5
    if (payload[index_processed] && payload[index_processed].input) payload = payload[index_processed].input

    const options = {
        uri: `${cfg.getConfig().env.ratingUrl}/${lob}`,
        // uri: `https://rateplatformservice-qa01.preprod.ent-rating-01.aws.ent.corp/rateplatformservice/rate/${lob}`,
        method: 'POST',
        json: true,
        rejectUnauthorized: false,
        body: payload
    }
    return api.restApi(options, 'rating-response.json', 'Rating Service')
}

function callRatingServiceProperty(file) {
    return utils.readJsonPromise(file)
        .then(data => callRatingService(data, 'property'))
        .catch(err => console.error(`Failed to call Rating Service: ${err}`))
}

function callRatingServiceAuto(file) {
    return utils.readJsonPromise(file)
        .then(data => callRatingService(data, 'auto'))
        .catch(err => console.error(`Failed to call Rating Service: ${err}`))
}

exports.submitTransferPayload = submitTransferPayload
exports.callRatingServiceProperty = callRatingServiceProperty
exports.callRatingServiceAuto = callRatingServiceAuto
