/**
* TODO : Read auth secrets from config file. 
*/

const rp = require('request-promise')
const cfg = require('./config.js')
const utils = require('./utils')

require('dotenv').config({ path: process.env.HOME + '/.e1p/.config' })

const { v4: uuidv4 } = require('uuid')
const { env } = require('process')

// Get CR Auth Token
const getCRAuthToken = () => {
    if (!process.env.CR_SECRET) {
        throw new Error('CompRater Auth secret not configured')
    }
    const authOptions = {
        uri: 'https://p2homesite.auth0.com/oauth/token',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: process.env.CR_SECRET
    };

    return rp(authOptions)
        .then(response => {
            const jsonToken = JSON.parse(response)
            return jsonToken.access_token;
        })
}

const getAuthToken = (app) => {

    // For PROD env, use a Auth token externally generated. 
    if (cfg.isProd()) {
        return utils.promptToken()
    }

    const options = {
        uri: `${cfg.getConfig().env.sapiHost}/v1/sapi-auth/authToken?authApplication=${app}`,
        method: 'GET',
        headers: {
            'X-Request-Id': cfg.getConfig().requestId
        }
    }
    return rp(options)
        .then(response => {
            const jsonToken = JSON.parse(response)
            return jsonToken.authToken;
        })
}

const getOeAuthToken = () => getAuthToken('OE')
const getGWAuthToken = () => getAuthToken('GW')
const getMSAAuthToken = () => getAuthToken('MSA')

exports.getAuthToken = getCRAuthToken
exports.getOeAuthToken = getOeAuthToken
exports.getGWAuthToken = getGWAuthToken
exports.getMSAAuthToken = getMSAAuthToken


