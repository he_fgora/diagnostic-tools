
const utils = require('./utils')
const cfg = require('./config')
const api = require('./api')

// Call PC to get the latest PM for requested LOB. 

function getProductModel(lob, state) {

    let lobMap = new Map()
    lobMap.set('HO3', '1')
    lobMap.set('HO4', '2')
    lobMap.set('HO6', '3')
    lobMap.set('HF9', '4')
    lobMap.set('AU', '5')

    let paLobMap = new Map()
    paLobMap.set('HO3', '1130422')
    paLobMap.set('HO4', '2130422')
    paLobMap.set('HO6', '3130422')
    paLobMap.set('HF9', '4130422')
    paLobMap.set('AU', '9')


    const product = (lob == 'AU') ? 'MSAAuto' : 'MSAProperty'
    const version = (state != 'PA' && state != 'UT') ? lobMap.get(lob) : paLobMap.get(lob)

    // const baseUrl = 'https://pc-qa-e1p.amfam.dev-1.us-east-1.guidewire.net/rest/product-model-api/v1/products'
    const baseUrl = cfg.getConfig().env.pmUrl

    const date = utils.todaysDate()
    const uri = `${baseUrl}/${product}/versions/${version}?state=${state}&asOfDate=${date}`

    const options = {
        uri: uri,
        method: 'GET',
        json: true,
        headers: {
            authorization: 'Basic c2Fsc3J2dDpSR2FpeXpEMzRvTVRsdThX'
        }
    }

    const file = `PM-${lob}-${state}.json`
    return api.restApi(options, file, `Product Model ${lob} ${state}`)
}

function getSpecificProductModel() {
    return getProductModel('HO3', 'TN')
}

function getAllProductModels() {
    lobs = ['HO3', 'HO4', 'HO6', 'HF9', 'AU']
    states = ['IL', 'IN', 'UT', 'PA']
    // states = ['PA']
    // states = ['UT']

    var promises = []

    lobs.forEach(lob => {
        states.forEach(state => promises.push(getProductModel(lob, state)))
    });

    console.log(`Promises count: ${promises.length}`)
    return Promise.all(promises)
        .then('Executed all promises')
}

exports.getAllProductModels = getAllProductModels
// exports.getAllProductModels = getSpecificProductModel