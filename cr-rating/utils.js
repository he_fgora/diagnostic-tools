const fs = require('fs')
const dt = require('date-and-time')
var inquirer = require('inquirer')


// Return A file and return a Promise the Json Object.
const readJsonPromise = (file) => fs.promises.readFile(file).then(JSON.parse)

// Return a substring in a string delimited by chr values 
const getSubStr = (str, chr) => str.substring(str.indexOf(chr) + 1, str.lastIndexOf(chr))

// Jsonify string
function jsonify(str) {
    try {
        return JSON.parse(str)
    } catch (e) {
        return str
    }
}

// Prefix file name
const prefixFile = (name, prfx) => `${prfx}-${name}`

// Delay a promise by delay ms
const delayPromise = (delay) => new Promise((resolve) => setTimeout(resolve, delay))

// Random number 
const randomInt = max => Math.floor(Math.random() * max)

// Randome Delay
const randomDelayPromise = max => delayPromise(randomInt(max) * 1000)

// Stringfy json
const stringify = (json) => JSON.stringify(json, null, 3)

// Formatted date in YYYY-MM-DD format. 
const formattedDate = (date) => dt.format(date, 'YYYY-MM-DD')

const todaysDate = () => formattedDate(new Date())

const prevDate = (days) => { const now = new Date(); now.setDate(now.getDate() - days); return formattedDate(now); }

const futureDate = (days) => { const now = new Date(); now.setDate(now.getDate() + days); return formattedDate(now); }

// Return a promise which prompts for Token & returns a promise for token entered by user. 
function promptToken() {
    return inquirer.prompt([
        {
            name: 'token',
            message: 'Enter Auth Token: '
        }
    ]).then(answers => answers.token)
}


exports.readJsonPromise = readJsonPromise
exports.getSubStr = getSubStr
exports.jsonify = jsonify
exports.prefixFile = prefixFile
exports.delayPromise = delayPromise
exports.randomInt = randomInt
exports.randomDelayPromise = randomDelayPromise
exports.stringify = stringify
exports.formattedDate = formattedDate
exports.todaysDate = todaysDate
exports.prevDate = prevDate
exports.futureDate = futureDate
exports.promptToken = promptToken