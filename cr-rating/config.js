const envConfig = require('./env-config.js')
const { v4: uuidv4 } = require('uuid')

// Global config value, used by all the modes. 
let g_config

function initConfig(options) {

    let envCfg = envConfig.envConfig.QA
    switch (options.env) {
        case 'UAT': envCfg = envConfig.envConfig.UAT
            break
        case 'PERF': envCfg = envConfig.envConfig.PERF
            break
        case 'LOCAL': envCfg = envConfig.envConfig.LOCAL
            break
        case 'STG': envCfg = envConfig.envConfig.STG
            break
        case 'PROD': envCfg = envConfig.envConfig.PROD
            break
        case 'INT': envCfg = envConfig.envConfig.INT
            break
        default: envCfg = envConfig.envConfig.QA
    }

    let offerSetId = null
    const list = [options.transfer, options.transferOptimizedDTO, options.offer]
    list.every(value => {
        offerSetId = value ? value : null
        return offerSetId == null
    })

    let acordXmlFile = null
    const acordList = [options.rate, options.acord]
    acordList.every(value => {
        acordXmlFile = value ? value : null
        return acordXmlFile == null
    })

    // Initialize the config object from the given input
    const config = {
        acordXmlFile: acordXmlFile,
        outputFolder: options.output,
        offerSetId: offerSetId,
        inquiryId: options.answers,
        invocationId: options.invocation,
        submissionId: options.submission,
        dtoFiles: options.compare,
        requestId: uuidv4(),
        msaClientId: 'e5m3lJ4l8ZxuNp5QjtVWJoPAg3fKsfq3',
        writeToFile: true,
        env: envCfg, 
        ff: options.featureFlagPM ? 'ff_useProductModelForBaseTemplate' : null
    }

    // initialize output folder if not given to acord xml path
    if (!options.output) {
        if (config.acordXmlFile)
            config.outputFolder = config.acordXmlFile.slice(0, config.acordXmlFile.lastIndexOf('/') + 1)
        else
            config.outputFolder = './output/'
    }

    // initialize global config value
    g_config = config
    return g_config
}

const getConfig = () => g_config
const isProd = () => (g_config.env === envConfig.envConfig.PROD)

exports.initConfig = initConfig
exports.getConfig = getConfig
exports.isProd = isProd