/**
 * Testing Transfer DTO for various LOB. 
 * 
 * 
 */

const fs = require('fs')
const rp = require('request-promise')

const cfg = require('./config')
const utils = require('./utils')
const rating = require('./rating')

const outputFile = 'transfer-dto-result.json'
const offerSetIDsFile = 'offerset-ids.json'

const options = {

}

// Get list of Successfull Rating requets for HO. 
function getRecentRatings(count) {

    const enteredDate = utils.prevDate(10)
    console.log(`Entered Date: ${enteredDate}`)
    const searchBody = {
        "query": {
            "offer.enteredDate": {
                "start": enteredDate
            },
            "carrier": "midvale.e1p.home",
            "offer.eligibility": ["OK"]
        },
        "paging": {
            "skip": 0,
            "take": count,
            "orderBy": [
                {
                    "fieldName": "offer.enteredDate",
                    "ascending": false
                }
            ]
        }
    }

    const options = {
        uri: `${cfg.getConfig().env.sapiHost}/v1/sapi-search/search`,
        method: 'POST',
        json: true,
        headers: {
            'X-Client-Id': cfg.getConfig().msaClientId,
            'X-Request-Id': cfg.getConfig().requestId
        },
        body: searchBody
    }
    return rp(options)
}

/**
 * Parse the Ids [offserSetId and inquiryId] from sapi-search result.
 * @param {*} search 
 * @returns list of {offerSetId, inquiryId}
 */
function parseIds(search) {
    return search.offers.map(element => {
        return {
            offerSetId: element.offersetId,
            inquiryId: element.inquiryId
        }
    });
}


/**
 * Given list of  get the offerSetId retrieve the Property LOB info from inquiry. 
 * @param {*} list : list of {offerSetId and inquiryId}
 * @returns list of Promises which would resturn : {offerSetId, lob}
 */
function getLOBInformation(list) {
    const promises = list.map(e => {
        return rating.getInquiryAnswers(e.inquiryId, true)
            .then(result => {
                return {
                    offerSetId: e.offerSetId,
                    lob: result.answers['property.policyType']
                }
            })
            .catch(err => {
                console.error(`Failed to get answers for : ${e.offerSetId}. Err: ${err}`.yellow)
                return {
                    offerSetId: e.offerSetId,
                    lob: ''
                }
            })
    })
    return Promise.all(promises)
        .then(searchLobList => {
            console.log(searchLobList)
            return searchLobList
        })
}

/**
 * Filter request lob from the search result
 * @param {list} searchLobList : Search result : list of [{offerSetId, lob}, ..]
 * @param {list} requestedLob  : Requested lob list 
 *                  HO3 :  ['01', '02', '03', '05'] - HOME OWNER
 *                  HO4 : ['04']  -         RENTERS
 *                  HO6 : ['06'] -          CONDO
 *                  HF9 : ['SP', 'BA'] -    SEASONAL
 * @returns [offerSetId,...]
 */
function filterLob(searchLobList, requestedLob) {
    const filteredLobList = searchLobList.filter(it => requestedLob.indexOf(it.lob) > -1)
    console.log(filteredLobList)
    return filteredLobList
}

/**
 * Parse the Rating IDs from filtered lob list.
 * @param {list} ratingIdList : [{offerSetId, lob},...]
 * @returns 
 */

function writeRatingIdsToFile(ratingIdList) {
    const data = JSON.stringify(ratingIdList, null, 4)

    const outputFilePath = cfg.getConfig().outputFolder + '/' + offerSetIDsFile
    fs.promises.writeFile(outputFilePath, data, 'utf8')
        .then(console.log(`OfferSetIDs (count: ${ratingIdList.length}) written to : ` + offerSetIDsFile))
    return ratingIdList
}

/**
 * Write results to a file.  
 */
function saveResultNew(id, useOptimizedDTO, result, error) {
    const file = cfg.getConfig().outputFolder + '/' + outputFile
    const data = { id: id, useOptimizedDTO: useOptimizedDTO, result: result, error }
    fs.appendFileSync(file, JSON.stringify(data) + `,\n`)
}

// Get the transfer promie to be executed later. 
function getTransferPromise(id, useOptimizedDTO) {
    return rating.transfer(id, useOptimizedDTO, true)
        .then(saveResult(id, useOptimizedDTO, 'OK'))
        .catch(err => {
            saveResultNew(id, useOptimizedDTO, 'FAILED', err.message)
            console.log(`Error for : ${id} : ${err}`.red)
        })
}

/**
 * Test getting Rating IDs for supported lobs. 
 * @param {*} requestedLobList : [HO3, HO4,..]
 * @returns 
 */
function getRatingIdsFromSearch(requestedLobList) {

    // Initialize the map of HO types and their local values
    let lobMap = new Map()
    lobMap.set('HO3', ['01', '02', '03', '05'])
    lobMap.set('HO4', ['04'])
    lobMap.set('HO6', ['06'])
    lobMap.set('HF9', ['SP', 'BA'])

    // Get flat list of lobs. 
    const requestedLobs = requestedLobList.flatMap(e => lobMap.get(e))

    const count = 5
    console.log(`Sample count: ${count},  Lobs: ${requestedLobs}`.yellow)

    return getRecentRatings(count)
        .then(result => parseIds(result))
        .then(ratingIds => getLOBInformation(ratingIds))
        .then(ratingIdLobList => filterLob(ratingIdLobList, requestedLobs))
        .then(filteredRatingIds => writeRatingIdsToFile(filteredRatingIds))
}

/**
 * Execute the The promises sequentially with a delay 
 * @param {list} ratingIdList  
 */
function transferSequentially(ratingIdList) {

    // Delay of 40 secs between each call, not to overwhelm
    const ms = 40000
    console.log(`Run transfers with delay : ${ms/1000} sec`.yellow)
    const reduced = ratingIdList.reduce((previousPromise, id) => {
        return previousPromise.then(() => {
            return getTransferPromise(id.offerSetId, true)
                .then(() => utils.delayPromise(ms))
        })
    }, Promise.resolve())
}

//  Initiate Transfers on Rating IDs gathered from sapi-search. 
function testTransferDTO() {

    const IdsFile = cfg.getConfig().outputFolder + '/' + offerSetIDsFile
    cfg.getConfig().writeToFile = false

    utils.readJsonPromise(IdsFile)
        .then(ids => transferSequentially(ids))
}


/** EXPORTS */

exports.getRecentRatings = getRatingIdsFromSearch
exports.testTransferDTO = testTransferDTO



/******* TODO: REMOVE THIS IF NOT REQUIRED ***********************************************************
 * 
 * Moving unused to bottom. Might require this for future reference. 
 */


let mapTransfer = new Map()

/**
*  Store the result in a map so that it can be logged later. 
* @param {string} id 
* @param {bool} useOptimizedDTO 
* @param {string} result 
*/
function saveResult(id, useOptimizedDTO, result, error) {
    let info = mapTransfer.get(id)
    if (!info) {
        info = {
            currentFlow: !useOptimizedDTO ? result : '',
            currentFlowError: !useOptimizedDTO ? error : '',
            optimizeDTOFlow: useOptimizedDTO ? result : '',
            optimizedDTOError: useOptimizedDTO ? result : '',
        }
    } else {
        if (useOptimizedDTO) {
            info.optimizeDTOFlow = result
            info.optimizedDTOError = error
        }
        else {
            info.currentFlow = result
            info.currentFlowError = error
        }
    }
    mapTransfer.set(id, info)
}

/**
 * Transfer each Rate response ID (offerSet ID)
 */
function transferFlows(ids) {
    // Delay max value in seconds. 
    const maxSecDelay = 60

    let delayTimeout = 10

    // Get the list of promises to execute an add random delay to each promise.
    const promises = ids.flatMap(id => {
        // random delay in ms
        // const random1 = utils.randomInt(maxSecDelay) * 1000  
        // const random2 = utils.randomInt(maxSecDelay) * 1000  
        const sec1 = delayTimeout * 1000
        const sec2 = (delayTimeout + 10) * 1000
        delayTimeout += 20

        return [
            utils.delayPromise(sec1).then(() => getTransferPromise(id, false)),
            utils.delayPromise(sec2).then(() => getTransferPromise(id, true))
        ]
    })

    console.log(`Number of promises : ${promises.length}`.yellow)

    // Execute the delayed promises
    Promise.all(promises)
        .then(_ => {
            mapTransfer.forEach((v, k) =>
                console.log(`${k}, ${v.currentFlow}, ${v.optimizeDTOFlow}`.green)
            )
            console.log(Object.fromEntries(mapTransfer))
        })
}

