/**
 * TODO: Handle Auto DTO comparision. 
 */

const fs = require('fs')

// Promise to read a file and return parsed JSON node. 
const readJsonPromise = (file) => fs.promises.readFile(file).then(JSON.parse)

// Compare rating DTO and transfer DTO Coverage information. 
// The coverage values should match. 
function compareDtos(ratingDtoFile, transferDtoFile) {

    // 
    return Promise.all([
            readJsonPromise(ratingDtoFile), 
            readJsonPromise(transferDtoFile)
        ])
        .then(results => {
            // Currently only testing for Property

            // Json Nodes for Coverages in rating and transfer DTO
            const ratingCoverages = results[0].context.policy.lobDataDTO.offerings[0].coverages.coverages;
            const gwCoverages = results[1].params[0].quoteData.lobData.homeowners_EH.offerings[0].coverages.coverages

            // Selected Coverages
            const ratingSelectedCoverages = ratingCoverages.filter(it => it.selected == 'true' || it.selected === true);
            const gwSelectedCoverages = gwCoverages.filter(it => it.selected === 'true' || it.selected === true )

            // Verify the coverages
            verifyCoverages(ratingSelectedCoverages, gwSelectedCoverages)

        })
        .catch(console.log)
}

// Compare selected coverages in rating and GW. 
function verifyCoverages(ratingCoverages, gwCoverages) {

    if (ratingCoverages.length !== gwCoverages.length) {
        console.error('Selected coverages count not equal. ');
        console.log('rating coverages: ' + ratingCoverages.length);
        console.log('gw coverages: ' + gwCoverages.length);
    }

    ratingCoverages.forEach(ratingCoverage => {
        console.log('\n'+ratingCoverage.codeIdentifier);
        var mathcingGwCoverages = gwCoverages.filter(gwItem => gwItem.codeIdentifier === ratingCoverage.codeIdentifier);
        if (mathcingGwCoverages.length == 0) {
            console.error("Didn't find the coverage in GW DTO : " + ratingCoverage.codeIdentifier);
        } else {
            // Found matching coverage. 
            // Check the term value if they exist. 
            if (ratingCoverage.terms.length > 0 && mathcingGwCoverages[0].terms.length > 0) {
                // console.log(ratingCoverage.codeIdentifier);
                console.log("rating term value: " + ratingCoverage.terms[0].chosenTermValue );
                // TODO: Check for directValue (if options don't exist) 
                if (mathcingGwCoverages[0].terms[0].chosenTerm) {
                    console.log("transfer term value: chosenTerm: " + mathcingGwCoverages[0].terms[0].chosenTerm );
                }
                if (mathcingGwCoverages[0].terms[0].directValue) {
                    console.log("transfer term value: directValue: " + mathcingGwCoverages[0].terms[0].directValue );
                }
            }
        }
    });

    console.log("--------------------------------------------------------------------------\n");

    gwCoverages.forEach(gwCoverage => {
        var matchingCoverages = ratingCoverages.filter(ratingCoverage => ratingCoverage.codeIdentifier === gwCoverage.codeIdentifier);
        if (matchingCoverages.length == 0) {
            console.error("Didn't find the coverage selected in Rating DTO : " + gwCoverage.codeIdentifier);
        }
    });

}

function compareGwDTOs( workingGwDTO, transferredGwDTO ) {
    return Promise.all([
        readJsonPromise(workingGwDTO), 
        readJsonPromise(transferredGwDTO)
    ])
    .then(results => {
        const workingCoverages = results[0].params[0].quoteData.lobData.homeowners_EH.offerings[0].coverages.coverages
        const transferredCoverages = results[1].params[0].quoteData.lobData.homeowners_EH.offerings[0].coverages.coverages
        
        // Selected Coverages
        const workingSelectedCoverages = workingCoverages.filter(it => it.selected == 'true' || it.selected === true);
        const transferredSelectedCoverages = transferredCoverages.filter(it => it.selected === 'true' || it.selected === true )

        verifyTransferCoverages(workingSelectedCoverages, transferredSelectedCoverages)

    })
}

function verifyTransferCoverages( workingCoverages, transferredCoverages ) {

    console.log('Working coverages: ' + workingCoverages.length);
    console.log('Transferred coverages: ' + transferredCoverages.length);

    if (workingCoverages.length !== transferredCoverages.length) {
        console.error('Selected coverages count not equal. ');
    }

    workingCoverages.forEach( workingCov => {
        console.log('\n' + workingCov.codeIdentifier);

        const matchingCoverages = transferredCoverages.filter(transferredCov => transferredCov.codeIdentifier === workingCov.codeIdentifier);
        if (matchingCoverages.length == 0) {
            console.error(`Didn't find the working coverage ${workingCov.codeIdentifier}  in transferred DTO.`);
        }
    })

}



exports.compareDtos = compareGwDTOs
// exports.compareDtos = compareDtos
