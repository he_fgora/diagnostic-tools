const envConfig = {
    'QA': {
        'compRaterHost': 'https://qae1pcomprater.homesitep2.com',
        'compRaterInternalHost': 'https://qa-enterpriseservices2.homesitep2.com', 
        'sapiAuth': 'https://qa-enterpriseservices.homesitep2.com',
        'sapiHost': 'https://qa-enterpriseservices.homesitep2.com',
        'oeHost': 'https://oe-qa.preprod.ent-rating-01.aws.ent.af1platform.com',
        'peUrl': 'https://producer-engage-amfam-e1p-qa.api.dev-1.us-east-1.guidewire.net/producer-engage',
        'pmUrl': 'https://pc-qa-e1p.amfam.dev-1.us-east-1.guidewire.net/rest/product-model-api/v1/products',
        'ratingUrl': 'https://rateplatformservice-qa01.preprod.ent-rating-01.aws.ent.corp/rateplatformservice/rate'
    },
    'INT': {
        'compRaterHost': 'https://qae1pcomprater.homesitep2.com', 
        'compRaterInternalHost': 'https://int-internal-sapi.homesitep2.com', 
        'sapiAuth': 'https://int-internal-sapi.homesitep2.com',
        'sapiHost': 'https://int-internal-sapi.homesitep2.com',
        'oeHost': 'https://oe-int.preprod.ent-rating-01.aws.ent.af1platform.com',
        'peUrl': 'https://producer-engage-amfam-e1p-int.api.dev-1.us-east-1.guidewire.net/producer-engage',
        'pmUrl': 'https://pc-int-e1p.amfam.dev-1.us-east-1.guidewire.net/rest/product-model-api/v1/products',
        'ratingUrl': 'https://rateplatformservice-int01.preprod.ent-rating-01.aws.ent.corp/rateplatformservice/rate'
    },
    'UAT': {
        'compRaterHost': 'https://uate1pcomprater.homesitep2.com',
        'compRaterInternalHost': 'https://uat-enterpriseservices2.homesitep2.com', 
        'sapiAuth': 'https://uat-enterpriseservices.homesitep2.com',
        'sapiHost': 'https://uat-enterpriseservices.homesitep2.com',
        'oeHost': 'https://oe-qa.preprod.ent-rating-01.aws.ent.af1platform.com',
        'peUrl': 'https://producer-engage-amfam-e1p-qa.api.dev-1.us-east-1.guidewire.net/producer-engage',
        'pmUrl': 'https://pc-qa-e1p.amfam.dev-1.us-east-1.guidewire.net/rest/product-model-api/v1/products',
        'ratingUrl': 'https://rateplatformservice-qa01.preprod.ent-rating-01.aws.ent.corp/rateplatformservice/rate'
    },
    'PERF': {
        'compRaterHost': 'https://qae1pcomprater.homesitep2.com', 
        'compRaterInternalHost': 'https://perf-internal-sapi.homesitep2.com', 
        'sapiAuth': 'https://perf-internal-sapi.homesitep2.com',
        'sapiHost': 'https://perf-internal-sapi.homesitep2.com',
        'oeHost': 'https://oe-perf.preprod.ent-rating-01.aws.ent.af1platform.com',
        'peUrl': 'https://producer-engage-amfam-e1p-qa.api.dev-1.us-east-1.guidewire.net/producer-engage'
    },
    'STG': {
        'compRaterHost': 'https://qae1pcomprater.homesitep2.com', 
        'compRaterInternalHost': 'https://stg-internal-sapi.homesitep2.com', 
        'sapiAuth': 'https://stg-internal-sapi.homesitep2.com',
        'sapiHost': 'https://stg-internal-sapi.homesitep2.com',
        'oeHost': 'https://oe-staging.preprod.ent-rating-01.aws.ent.af1platform.com',
        'peUrl': 'https://producer-engage-amfam-e1p-qa.api.dev-1.us-east-1.guidewire.net/producer-engage'
    },
    'PROD': {
        'compRaterHost': 'https://enterpriseservices2.homesitep2.com', 
        'compRaterInternalHost': 'https://enterpriseservices2.homesitep2.com', 
        'sapiAuth': 'https://enterpriseservices.homesitep2.com',
        'sapiHost': 'https://enterpriseservices.homesitep2.com',
        'oeHost': 'https://oe.ent-rating-01.aws.ent.af1platform.com',
        'peUrl': 'https://producer-engage-amfam-e1p-qa.api.dev-1.us-east-1.guidewire.net/producer-engage'
    },
    'LOCAL' : {
        'compRaterHost': 'https://qae1pcomprater.homesitep2.com',
        'compRaterInternalHost': 'http://localhost:8080', 
        'sapiAuth': 'http://localhost:8080',
        'sapiHost': 'http://localhost:8080',
        'oeHost': 'https://oe-qa.preprod.ent-rating-01.aws.ent.af1platform.com',
        'peUrl': 'https://producer-engage-amfam-e1p-qa.api.dev-1.us-east-1.guidewire.net/producer-engage'
    }
}

exports.envConfig = envConfig