
const rp = require('request-promise')
const fs = require('fs')

const cfg = require('./config')

/**
 * Base help function to make the Rest API Call and save the output to a file. 
 * @param {json} options
 * @param {string} outputFile
 * @param {string} requestName : 
 * @returns a promoise  with response from the rest api call.
 */

 function restApi(options, outputFile, requestName, noLog) {
    if (!noLog) console.log(`Calling ${requestName} : ${options.uri}`)

    const outputFilePath = cfg.getConfig().outputFolder + '/' + outputFile

    options.timeout = 35000
    // TESTING : options.timeout = 5000

    return rp(options)
        .then(response => {
            var prettyResponse = response
            if (options.json) {
                prettyResponse = JSON.stringify(response, null, 4)
            }
            if (cfg.getConfig().writeToFile) {
                fs.promises.writeFile(outputFilePath, prettyResponse, 'utf8')
                    .then(console.log(`${requestName} Response (${prettyResponse.length} Bytes) written to : ` + outputFile))
            }
            return response
        })
        .catch(err => {
            console.error(`Error calling ${requestName}, URI: ${options.uri}: ${err}`.red)
            if (err.cause.code === 'ETIMEDOUT') {
                console.error(`**********************************************`.red)
                console.error(`**** You must be signed into HOMESITE VPN ****`.red)
                console.error(`**********************************************`.red)
            }
            throw err
        })
}

exports.restApi = restApi