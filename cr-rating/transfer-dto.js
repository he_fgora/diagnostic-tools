/**
 * Run IT tests Sequentially 
 * Get a Rating ID
 * Check if this matches the requested LOB. 
 * 
 */


const { coroutine } = require('bluebird')
const rp = require('request-promise')
const fs = require('fs')

const cfg = require('./config')
const utils = require('./utils')
const rating = require('./rating')

const DEFAULT_SEARCH_SAMPLE = 500
const DEFAULT_DELAY = 60 // seconds
const DEFAULT_DAYS_BEHIND = 5 // days old data

// Get a Rating ID. 
function getRatingIDs(searchIndex, count, lob) {

    // Start data about 50 days old
    const enteredDate = utils.prevDate(DEFAULT_DAYS_BEHIND)
    const carrier = (lob == 'auto') ? 'midvale.e1p.auto' : 'midvale.e1p.home'
    if (!count) count = 1

    // If no searchIndex given then randomize the index to get different offerset ids.
    if (!searchIndex) searchIndex = utils.randomInt(900)

    // Search for Offers which have succeeded for a given LOB type.  
    const searchBody = {
        "query": {
            "offer.enteredDate": {
                "start": enteredDate
            },
            "carrier": carrier,
            "offer.eligibility": ["OK"]
        },
        "paging": {
            "skip": searchIndex,
            "take": count
        }
    }

    const options = {
        uri: `${cfg.getConfig().env.sapiHost}/v1/sapi-search/search`,
        method: 'POST',
        json: true,
        headers: {
            'X-Client-Id': cfg.getConfig().msaClientId,
            'X-Request-Id': cfg.getConfig().requestId
        },
        body: searchBody
    }
    return rp(options)
}

// Parse the Ids [offserSetId and inquiryId] from sapi-search result.
function parseIds(search) {
    return search.offers.map(element => {
        return {
            offerSetId: element.offersetId,
            inquiryId: element.inquiryId
        }
    });
}

// Given {offerSetId, inquiryId} get the LOB info from inquiry
// Return : {offerSetId , LOB}
function getLOBInfoFromInquiry(ids) {
    const promises = ids.map(e => {
        return rating.getInquiryAnswers(e.inquiryId, true)
            .then(result => {
                return {
                    offerSetId: e.offerSetId,
                    lob: result.answers['property.policyType']
                }
            })
            .catch(err => {
                console.error(`Failed to get answers for : ${e.offerSetId}. Err: ${err}`.yellow)
                return {
                    offerSetId: e.offerSetId,
                    lob: ''
                }
            })
    })
    return Promise.all(promises)
        .then(searchLobList => {
            console.log(searchLobList)
            return searchLobList
        })
}

// Given [{id, lob}] filter the requested lob.
// returns : [{id, lob}]
function filterRequestedLob(lobs, requestedLob) {
    return lobs.filter(it => requestedLob.indexOf(it.lob) > -1)
}

// Given LOB get rating ID (or list) in sapi-search. 
function getRequestedRatingIds(searchIndex, requestedLobCodes) {

    return getRatingIDs(searchIndex)
        // sapi search result
        .then(result => parseIds(result))
        // result : [{offerSetId , inquiryId},,]
        .then(id => getLOBInfoFromInquiry(id))
        // {id, lob} filters to requested lob.
        .then(lobs => filterRequestedLob(lobs, requestedLobCodes))
    // returns filtered {offerSetId, lob}
}


const transferResultFile = 'transfer-dto-result.json'
// Append the result to a file. 
function saveResult(id, lob, error) {
    const data = { id: id, lob: lob, result: error }
    const file = cfg.getConfig().outputFolder + '/' + transferResultFile
    fs.appendFileSync(file, JSON.stringify(data) + `,\n`)
}

// Run transfer with optimized Flow & log the result
function transferAndLog(ratingIds) {
    const delay = DEFAULT_DELAY * 1000

    const id = ratingIds[0]
    if (id) {
        return rating.justTransfer(id.offerSetId, true)
            .catch(err => {
                saveResult(id.offerSetId, id.lob, err.message)
                console.log(`Error for : ${id} : ${err}`.red)
            })
            .then(() => utils.delayPromise(delay))
    } else {
        return Promise.resolve()
    }
}

// Search for requested LOB codes and then transfer
function searchAndTransfer(searchIndex, requestedLobCodes) {
    return getRequestedRatingIds(searchIndex, requestedLobCodes)
        .then(filteredId => transferAndLog(filteredId))
        .catch(err => console.error(`Error Search and Transfer : ${err.message}`.red))
}

// Search and Transfer all HO types. 
// Without querying Answers to figure out what the 
function searchAndTransferLOB(lob) {
    return getRatingIDs(null, 1, lob)
        .then(searchResponse => parseIds(searchResponse))
        .then(ratingIds => transferAndLog(ratingIds))
        .catch(err => console.error(`Error Search and Transfer : ${err.message}`.red))
}


// Run the test sequentially for requested LOBs (HO3, HO4...)
// count indicates how many searches to run. 
/*                 HO3 :  ['01', '02', '03', '05'] - HOME OWNER
*                  HO4 : ['04']  -                   RENTERS
*                  HO6 : ['06'] -                    CONDO
*                  HF9 : ['SP', 'BA'] -              SEASONAL
*/
function transferSequentially(requestedLobList, count) {

    // disable too much writing to files 
    cfg.getConfig().writeToFile = false

    // If count not given the default it. 
    if (!count) count = DEFAULT_SEARCH_SAMPLE

    // Initialize the map of HO types and their local values
    let lobMap = new Map()
    lobMap.set('HO3', ['01', '02', '03', '05'])
    lobMap.set('HO4', ['04'])
    lobMap.set('HO6', ['06'])
    lobMap.set('HF9', ['SP', 'BA'])

    // Get flat list of lobs. 
    const requestedLobCodes = requestedLobList.flatMap(e => lobMap.get(e))
    console.log(`LOB codes ${requestedLobCodes}`)

    const array = Array.from(Array(count).keys());

    array.reduce((previousPromise, v) => {
        return previousPromise.then(() => {
            return searchAndTransfer(null, requestedLobCodes)
        })
    }, Promise.resolve())
}

// Run transfer on all HO types. 
function integrationTestLOB(lob) {

    const searchCount = DEFAULT_SEARCH_SAMPLE
    const array = Array.from(Array(searchCount).keys());

    array.reduce((previousPromise, v) => {
        return previousPromise.then(() => {
            return searchAndTransferLOB(lob)
        })
    }, Promise.resolve())
}

const integrationTestHO = () =>  integrationTestLOB('home')
const integrationTestAU = () =>  integrationTestLOB('auto')

// Exports
exports.transferDTOTest = transferSequentially
exports.searchAndTransferHO = integrationTestHO
exports.searchAndTransferAU = integrationTestAU