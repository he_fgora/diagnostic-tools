const fs = require('fs')
const auth = require('./auth-token')
const xml2js = require('xml2js')

const colors = require('colors')

const api = require('./api')
const oe = require('./oe')
const cfg = require('./config')
const utils = require('./utils')

/**
 * Configurations
 */

// Create this by deffault where the input file exist. 
const acordResponseFile = 'acord-response.xml'
const offerResponseFile = 'offer-response.json'
const answersInquiryResponseFile = 'inquiry-answers.json'
const oeInvocationFile = 'oe-invocation-rating.json'
const oeInvocationDomainSrvsCallsFile = 'oe-invocation-domain-srvs-calls.json'

const transferInvocationFile = 'transfer-oe-invocation.json'
const transferResponseFile = 'transfer-response.json'
const transferSubmissionInfoFile = 'transfer-submission-info.json'

// Rate Request. 
function rateRequest(authToken, xmlFile) {

    const xmlData = fs.readFileSync(xmlFile, 'utf8')

    let url = `${cfg.getConfig().env.compRaterInternalHost}/v1/e1p-comprater/rate`
    if (cfg.getConfig().ff) 
        url += `?${cfg.getConfig().ff}=true`

    const options = {
        // Enable this to use public Comprater End point in QA only. This requires 
        // token to be generated. 
        // uri: `${cfg.getConfig().env.compRaterHost}/v1/e1p-comprater/rate`,

        // Turn on Refactored Bumping service code.  
        // uri: `${cfg.getConfig().env.compRaterInternalHost}/v1/e1p-comprater/rate?ff_useRefactoredBumpingService=true`,

        // Internal Comprater End point. Does not require any Token generation. 
        // Need to be signed into Homesite VPN. 
        //uri: `${cfg.getConfig().env.compRaterInternalHost}/v1/e1p-comprater/rate?ff_useProductModelForBaseTemplate=true`,
        uri: url,
        method: 'POST',
        headers: {
            'Content-Type': 'application/xml',
            'X-Request-Id': cfg.getConfig().requestId,
            // For internal host don't need token. 
            // Authorization: `Bearer ${authToken}`,
            // This is required if using internal host. 
            'X-Client-Id': cfg.getConfig().msaClientId
        },
        body: xmlData
    }
    return api.restApi(options, acordResponseFile, 'Rating')
}


function rateWithToken(acordXmlFile) {
    return auth.getAuthToken()
        .then(token => rateRequest(token, acordXmlFile))
}

// Parse inquiry Id & offerset Id. 
function parseAcordResponse(response) {
    const parser = new xml2js.Parser({ attrkey: "ATTR" });
    return parser.parseStringPromise(response)
        .then(result => {

            const statusCd = result.ACORD.Status[0].StatusCd[0]
            console.log(`Rating Status Code ${statusCd}`)

            const info = (result.ACORD.InsuranceSvcRs[0].PersAutoPolicyQuoteInqRs != null) ?
                result.ACORD.InsuranceSvcRs[0].PersAutoPolicyQuoteInqRs[0].ItemIdInfo[0] :
                result.ACORD.InsuranceSvcRs[0].HomePolicyQuoteInqRs[0].ItemIdInfo[0];

            const offerSetId = info.SystemId[0]
            const inquiryId = info.InquiryId[0]
            const inquiryInvocationId = info.InvocationId[0]

            console.log(`inquiryId : ${inquiryId}`)
            console.log(`offerSetId : ${offerSetId}`.green)
            console.log(`inquiryInvocationId : ${inquiryInvocationId}`)

            return [offerSetId, inquiryId, inquiryInvocationId]
        })
}

// Get inquiry-answers
function getInquiryAnswers(inquiryId, noLog) {
    const options = {
        uri: `${cfg.getConfig().env.sapiHost}/v1/sapi-inquiry/inquiries/${inquiryId}/answers`,
        method: 'GET',
        json: true,
        headers: {
            'X-Client-Id': cfg.getConfig().msaClientId,
            'X-Request-Id': cfg.getConfig().requestId
        }
    }
    return api.restApi(options, answersInquiryResponseFile, 'InquiryAnswers', noLog)
}

// Get offerset
function getOfferSet(offerSetId) {
    const options = {
        uri: `${cfg.getConfig().env.sapiHost}/v1/sapi-offers/offers/${offerSetId}`,
        method: 'GET',
        json: true,
        headers: {
            'X-Client-Id': cfg.getConfig().msaClientId,
            'X-Request-Id': cfg.getConfig().requestId
        }
    }
    return api.restApi(options, offerResponseFile, 'Offer')
}

// Parse invocation ID from offer response. 
function parseInvocationId(offerResponse) {
    const root = offerResponse.offers['midvale.e1p.home'] != null ?
        offerResponse.offers['midvale.e1p.home'] : offerResponse.offers['midvale.e1p.auto']

    if (!root.premiums) {
        throw new Error('Offer response does not have oeInvocation')
    }
    const oeInvocationId = root.premiums[0].carrierId
    console.log(`OE InvocationId : ${oeInvocationId}`)
    return oeInvocationId
}

// Rating, GetInquiry, GetOfferSet, GetInvocation info. 
function rateAndGatherInfo(config) {
    console.log('Start : Rate & Transfer Request')

    return rateRequest(null, config.acordXmlFile)
        .then(acordResponse => parseAcordResponse(acordResponse))
        .then(ids => {
            const offerSetId = ids[0]
            const inquiryId = ids[1]
            const invocationDomainCallsId = ids[2]

            // Get offerSet and invocation
            // Add a delay as the OfferSetID might be no yet available in database. 
            utils.delayPromise(1000)
                .then(() => getOfferSet(offerSetId))
                .then(parseInvocationId)
                // Get OE Invocation.
                .then(oeInvocationId => oe.getOeInvocation(oeInvocationId, oeInvocationFile))
                .catch(err => console.error(`Failed OfferSet: ${err}`.red))


            // Get Inquiry Answers
            getInquiryAnswers(inquiryId)
                .catch(err => console.error(`Failed getInquiryAnswers: ${err}`.red))

            // Get OE invocation for Domain srvs calls. 
            oe.getOeInvocation(invocationDomainCallsId, oeInvocationDomainSrvsCallsFile)
                .catch(err => console.error(`Failed Oe Invocation for Domain Srvc calls: ${err}`.red))

            // Transfer & get submission and OE invocation
            transfer(offerSetId, true)
                .catch(err => console.error(`Failed transfer: ${err}`.red))
        })
        .catch(err => {
            console.error(`Error Executing rateAndGatherInfo: ${err.message}`.red)
        })
}

function justTransfer(offerSetId, optimizeDTO) {
    let uriParam = `${cfg.getConfig().env.sapiHost}/v1/sapi-transfer/transfer/${offerSetId}?ff_useOeManifest=true`
    if (optimizeDTO)
        uriParam += '&ff_useOptimizedDTO=true'
    else 
        uriParam += '&ff_useOptimizedDTO=false'

    const options = {
        uri: uriParam,
        method: 'PUT',
        json: true,
        headers: {
            'X-Client-Id': cfg.getConfig().msaClientId,
            'X-Request-Id': cfg.getConfig().requestId,
            'Afe-Session-Id': cfg.getConfig().requestId
        }
    }
    return api.restApi(options, transferResponseFile, 'Transfer')
        .then(parseTransferResponse)
}

// Transfer request
function transfer(offerSetId, optimizeDTO) {
    return justTransfer(offerSetId, optimizeDTO)
        .then(response => {
            getGetTransferInfo(response[0], transferSubmissionInfoFile)
            if (response[1]) {
                oe.getOeInvocation(response[1], transferInvocationFile)
            } else {
                console.error('Did not get the invocation ID from Transfer request'.yellow)
            }
        })
}

// Parse transfer response for submissionId and oeInvocationId
function parseTransferResponse(response) {
    const submissionId = response.result.submissionID
    const oeInvocationId = response.invocationId
    console.log(`Submission Id : ${submissionId}`.green)
    console.log(`Transfer OE Invocation ID: ${oeInvocationId}`)

    if (response.product.endsWith('auto')) {
        console.log(`Bridge URL: ${cfg.getConfig().env.peUrl}/quote-ea?jobNumber=${submissionId}`.green)
    } else if (response.product.endsWith('home')) {
        console.log(`Bridge URL: ${cfg.getConfig().env.peUrl}/quote-eh?jobNumber=${submissionId}`.green)
    } else {
        console.error(`Unknown product in transfer response: ${response.product}`.red)
    }
    return [submissionId, oeInvocationId]
}

// Get Submission ID information. 
function getGetTransferInfo(submissionId, fileName) {
    const options = {
        uri: `https://producer-engage-amfam-e1p-qa.api.dev-1.us-east-1.guidewire.net/gatewayquote/quote`,
        method: 'POST',
        rejectUnauthorized: false,
        json: true,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'X-Request-Id': cfg.getConfig().requestId,
            'X-Client-Id': cfg.getConfig().msaClientId,
            Authorization: 'Basic c2Fsc3J2dDpSR2FpeXpEMzRvTVRsdThX'
        },
        body: {
            id: 'ce81eeb5-f748-44fc-9602-7d97d3fbcc8c15',
            jsonrpc: '2.0',
            method: 'retrieve',
            params: [
                {
                    quoteID: submissionId
                }
            ]
        }
    }
    const submissionFile = fileName ? fileName : transferSubmissionInfoFile
    return api.restApi(options, submissionFile, 'Transfer Submission Info')
        .catch(err => console.error(`Failed to Get Transfer Submission Info : ${err.message}`.red))
}

// EXPORTS 

exports.rateAndGatherInfo = rateAndGatherInfo
exports.justTransfer = justTransfer
exports.transfer = transfer
exports.getInquiryAnswers = getInquiryAnswers
exports.getOfferSet = getOfferSet
exports.getGetTransferInfo = getGetTransferInfo
