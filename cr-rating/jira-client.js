var JiraApi = require('jira-client')

jira = new JiraApi({
    protocol: 'https',
    host: 'amfament.atlassian.net',
    username: 'he_fgora@homesite.com',
    password: '****',
    apiVersion: 2,
    strictSSL: true
})

jira.findIssue('E1PRSA-4116')
    .then(issue => {
        console.log(`Status: ${issue.fields.status.name}`)
    })
    .catch(err => {
        console.error(err)
    })