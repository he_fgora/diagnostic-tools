const xml2js = require('xml2js')
const fs = require('fs')
const config = require('./config')
const utils = require('./utils')

const experienceId = 'SAPI.partner.experienceId'
const producerCode = 'SAPI.partner.producerCode'

// Check if the Acord has all required information
function checkAcordValidity(xmlFile) {
    var xmlData = fs.readFileSync(xmlFile, 'utf8')

    const parser = new xml2js.Parser();
    return parser.parseStringPromise(xmlData)
        .then(result => {

            var updated = false

            // Check partner information 
            const svcReq = result.ACORD.InsuranceSvcRq[0]
            if (!svcReq[experienceId]) {
                console.error('Partner experienceId not set in Acord'.red)
                svcReq[experienceId] = ['7118']
                updated = true
            }
            if (!svcReq[producerCode]) {
                console.error('Parnter producerCode not set in Acord'.red)
                svcReq[producerCode] = [ '35601' ]
                updated = true
            }

            // Check effective Date. 
            const quoteInqRq = svcReq.HomePolicyQuoteInqRq ? svcReq.HomePolicyQuoteInqRq :
                svcReq.PersAutoPolicyQuoteInqRq
            if (!quoteInqRq)
                throw new Error("Invalid Acord : Couldn't find HomePolicyQuoteInqRq or PersAutoPolicyQuoteInqRq".red)

            const node = quoteInqRq[0].PersPolicy[0].ContractTerm[0]
            const effectiveDt = node.EffectiveDt[0]
            const expirationDt = node.ExpirationDt[0]

            console.log('EffectiveDt: ' + node.EffectiveDt[0])
            console.log('ExpirationDt: ' + node.ExpirationDt[0])

            const effDate = Date.parse(effectiveDt)
            if ( !effDate || effDate < Date.parse(utils.todaysDate())) {
                const futureDate = utils.futureDate(3)
                console.error(`Invalid EffectiveDt ${effectiveDt} or older than today: ${utils.todaysDate()}, Setting it future  date: ${futureDate} `.red)
                node.EffectiveDt[0] = futureDate
                updated = true
            }

            const expDate = Date.parse(expirationDt)
            if ( !expDate || expDate < Date.parse(utils.futureDate(30))) {
                const futureDate = utils.futureDate(120)
                console.error(`Invalid ExpirationDt ${effectiveDt} or is too short, Setting it to future date, say 120 days: ${futureDate} `.red)
                node.ExpirationDt[0] = futureDate
                updated = true
            }

            // If updated generate the new XML file with the fix. 
            if (!updated) return

            const builder = new xml2js.Builder()
            const xml = builder.buildObject(result)

            const outFile = config.getConfig().outputFolder + '/acord-updated.xml'  
            fs.writeFileSync(outFile, xml)
            console.log(`Updated the Acord : ${outFile}`)
        })
}

exports.checkAcordValidity = checkAcordValidity