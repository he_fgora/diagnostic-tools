/**
 * Get Version information of hte services. 
 */


const rp = require('request-promise');
const xml2js = require('xml2js');
const cfg = require('./config');
const { jsonify } = require('./utils');

let versionMap = new Map()


// Get Version information. 
function getVersion(host, gateway) {
    if (!gateway) gateway = cfg.getConfig().env.sapiHost
    const options = {
        uri: `${gateway}/v1/${host}/health/ping-app`,
        method: 'GET'
    }
    return rp(options)
        .then(response => parseVersion(host, response))
        .catch(err => console.log(`Failed to get version for : ${options.uri}, err: ${err}`.red))
}

// Parse version information
function parseVersion(host, response) {

    const parser = new xml2js.Parser({ attrkey: "ATTR" });
    return parser.parseStringPromise(response)
        .then(result => {
            const version = result.pingdom_http_custom_check.version[0]
            versionMap.set(host, version)
        })
        // Probably JSON data. 
        .catch(erro => {
            const json = jsonify(response)
            if( json.version ) versionMap.set(host, json.version)
        })
}

function getVersions() {
    // console.log(`Environment : ` + cfg.getConfig().env.sapiHost)

    // input for the getting the versions information. 
    const versions =
        [
            // service name , gateway to check for the info (defaults to sapi gateway)
            { srvc: 'e1p-comprater', gateway: cfg.getConfig().env.compRaterHost },
            { srvc: 'sapi-inquiry' },
            { srvc: 'sapi-offers' },
            { srvc: 'sapi-e1p-mapping-service', gateway: cfg.getConfig().env.compRaterInternalHost },
            { srvc: 'sapi-e1p-auto-offers' },
            { srvc: 'sapi-e1p-home-offers' },
            { srvc: 'sapi-transfer' },
            { srvc: 'sapi-e1p-transfer' },
            { srvc: 'sapi-search' }
        ]

    // List of promises which get the versions as configured above. 
    const promises = versions.map(e => getVersion(e.srvc, e.gateway))

    // Execute all the promises 
    Promise.all(promises)
        // After running all promises, output the info in the order mentioned 
        .then(_ => versions.forEach(
            v => console.log(`${v.srvc} : ${versionMap.get(v.srvc)}`)))
}


exports.getVersions = getVersions
