/**
 * Proces OE Invocation information into more readable stuff. 
 */

const fs = require('fs')
const rp = require('request-promise')
const fuzzy = require('fuzzysearch')

const utils = require('./utils')
const auth = require('./auth-token')
const cfg = require('./config')
const api = require('./api')

const debugFile = (file) => appendFileName(file, 'debug')
// Helper function to insert a string into the file 
// e.g invocation.json => invocation-debug.json
const appendFileName = (file, str) => file.slice(0, file.lastIndexOf('.')) + '-' + str + file.slice(file.lastIndexOf('.'))
const processedFile = (file) => appendFileName(file, 'processed')

/**
 * Read OE Debug invocation and create following for each task  : 
 * Task 
 * URL request 
 * input payload
 * output returned
 */

function processOeInvocation(json, file) {

    var taskList = []
    json.forEach(task => {
        if (task.message.includes('executed')) {
            const data = {
                taskName: utils.getSubStr(task.message, "'"),
                url: task.url,
                input: utils.jsonify(task.details.request.data),
                output: utils.jsonify(task.details.response.payload)
            }
            taskList.push(data)
        }
    });
    fs.promises.writeFile(file, JSON.stringify(taskList, null, 4), 'utf8')
        .then(console.log(`OE Processed information written to : ` + file))
}

// helper function to get manifest call. 
function getOptions(token, manifest) {

    const uri = cfg.getConfig().env.oeHost
    const options = {
        uri: uri,
        method: 'GET',
        rejectUnauthorized: false, // Add this to avoid the error : UNABLE_TO_GET_ISSUER_CERT_LOCALLY, usually happens in VPN
        json: true,
        headers: {
            'X-Request-Id': cfg.getConfig().requestId,
            Authorization: `Bearer ${token}`,
            'User-Agent': 'Request-Promise'
        }
    }                    
    return options
}

// Get list of manifest matching given pattern. 
function getOEManifestNames(pattern) {
    const isPattern = (typeof pattern === 'string')
    return auth.getOeAuthToken()
        .then(token => { 
            const options = getOptions(token)
            options.uri += '/manifests'
            return rp(options)
                .then(listManifest => {
                    const output = listManifest.map(m => m.name).
                        // Filter only if paramter is given
                        filter(name => isPattern ? fuzzy(pattern.toLowerCase(), name.toLowerCase()): true)
                    console.log(output)
                })
            }
        )
}

// Get a manifest file. 
function getOEManifest(manifest) {
    return auth.getOeAuthToken()
        .then(token => { 
            const options = getOptions(token, manifest)
            options.uri += `/manifests/${manifest}`
            return api.restApi(options, manifest + '.json', 'Get Manifest: ')})
}

// Parse the recent invocations result and output useful info
function parseRecentInvocations(invocations) {
    const invs = invocations.map(inv => {
        const output = {
            id: inv.id,
            invoked: new Date(inv.createdAt).toLocaleString(),
            timeSpentMS: inv.executeDurationMs.toLocaleString("en-US") 
        }
        return output
    })

    console.log(invs)
}

// Get recent invocations on a manifest. 
function getRecentInvocations(manifest) {
    return auth.getOeAuthToken() 
        .then(token => {
            const options = getOptions(token, manifest)
            options.uri += `/invocations/${manifest}/recent?limit=10`
            return rp(options)
                .then(invocations => parseRecentInvocations(invocations))
        })
}

//  Get OE Invocation information given the token.
function getOeInvocationWithToken(token, invocationId, responseFile, debug) {
    const uri = `${cfg.getConfig().env.oeHost}/invocations/${invocationId}` + (debug ? '/debug' : '')
    const options = {
        uri: uri,
        method: 'GET',
        rejectUnauthorized: false, // Add this to avoid the error : UNABLE_TO_GET_ISSUER_CERT_LOCALLY, usually happens in VPN
        json: true,
        headers: {
            'X-Request-Id': cfg.getConfig().requestId,
            Authorization: `Bearer ${token}`,
            'User-Agent': 'Request-Promise'
        }
    }
    const file = debug ? debugFile(responseFile) : responseFile
    const msg = 'OE Invocation' + (debug ? ' Debug' : '')
    return api.restApi(options, file, msg)
}

// Get OE Invocation after getting OE Token.
function getOeInvocation(oeInvocationId, responseFile) {
    if (!oeInvocationId) {
        throw new Error(`Invalid OE Invocation ID`.red)
    }
    return auth.getOeAuthToken()
        .then(token => Promise.all([
            getOeInvocationWithToken(token, oeInvocationId, responseFile),
            getOeInvocationWithToken(token, oeInvocationId, responseFile, true)
                .then(response => processOeInvocation(response, cfg.getConfig().outputFolder + '/' + processedFile(responseFile)))
        ]))
}


exports.processOeInvocation = processOeInvocation
exports.getOEManifestNames = getOEManifestNames
exports.getOEManifest = getOEManifest
exports.getRecentInvocations = getRecentInvocations
exports.getOeInvocation = getOeInvocation