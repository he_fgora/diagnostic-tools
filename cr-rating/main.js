const { program } = require('commander')
const fs = require('fs')
require('console-stamp')(console, 'HH:MM:ss.l')

const rating = require('./rating.js')
const cfg = require('./config.js')
const compare = require('./compare-dtos.js')
const version = require('./version')
const testDTO = require('./test-transfer-dto')
const transferDTO = require('./transfer-dto')
const services = require('./services')
const pm = require('./product-model')
const acord = require('./acord-xml')
const oe = require('./oe')

program
  .option('-o, --output  <output-folder>', 'Path to Output Folder to put output response. Defaults to Acord file"s folder')
  .option('-r, --rate  <request-acord.xml file>', 'Rate & Transfer : Path Acord Request XML')
  .option('-t, --transfer  <offerSetID>', 'OfferSet UUID.')
  .option('-T, --transferOptimizedDTO  <offerSetID>', 'Run Transfer thru new Optimize DTO flow.')
  .option('-a, --answers <inquiryId>', 'Get inquiry answers. inquiryId : UUID')
  .option('-f, --offer <offerSetId>', 'Get OfferSet Info. offerSetId : UUID')
  .option('-i, --invocation <oe-Invocation-ID>', 'Get OE Invocation ID : UUID')
  .option('-s, --submission <submssion ID>', 'Get transfer Submssion Information')
  .option('-c, --compare <DTOs...>', 'WIP : Compare DTO Coverages: RatingDTOFIle, TransferDTOFile')
  .option('-g, --getRatingIds <LOBTypes...>', 'Get Rating IDs (SystemIDs) from sapi-search for LOB types : HO3 HO4')
  .option('-d, --runTransferTest <LOBTypes...>', 'Run Transfer Tests for given LOBs (HO3, HO4, ..)')
  .option('-HO --runTransferHO', "Run Transfer HO tests")
  .option('-AU --runTransferAU', "Run Transfer AU tests")
  .option('-e, --env <environment>', 'QA (Default), UAT')
  .option('-gw, --submitGW <payloadFile>',  'Submit DTO to GW')
  .option('-rp, --ratingServiceProperty <payloadFile>',  'Call Rating Service for Property Directly')
  .option('-ra, --ratingServiceAuto <payloadFile>',  'Call Rating Service for Auto Directly')
  .option('-pm, --productModel', 'Get Product Models for all States/LOBs')
  .option('-x, --acord <acord.xml>', 'Check and Fix Acord for Rating')
  .option('-v, --versions ', 'Get Version of all the SAPI services')
  .option('-ml, --manifestList [pattern]', 'Get List of Manifests matching the pattern')
  .option('-m, --manifest <name>', 'Get Manifest given the name')
  .option('-is, --recentInvcs <manifest>', 'Get recent invocations of a manifest')
  .option('-ffPM --featureFlagPM', 'Pass this query param feature flag for ff_useProductModelForBaseTemplate')

program.parse(process.arg)

const options = program.opts();

// Initialize global config information 
const config = cfg.initConfig(options)
console.log(`Trace request ID : ${cfg.getConfig().requestId}`.blue)
console.log(`Output Folder : ${cfg.getConfig().outputFolder}`.yellow)

// Check input validity (path checks)
if (!checkValidity(config)) {
  return
}

// Rate and Transfer request. 
if (options.rate) {
  return rating.rateAndGatherInfo(config)
}

if (options.rate) {
  return rating.rateAndGatherInfo(config, true)
}

// If Transfer is requested. 
if (options.transfer) {
  return rating.transfer(config.offerSetId)
}

// Transfer Optimized flow 
if (options.transferOptimizedDTO) {
  return rating.transfer(config.offerSetId, true)
}

// Get Inquiry information
if (options.answers) {
  return rating.getInquiryAnswers(config.inquiryId)
}

// Get Offer Set information
if (options.offer) {
  return rating.getOfferSet(config.offerSetId)
}

// Get Invocation ID
if (options.invocation) {
  return oe.getOeInvocation(config.invocationId, 'oe-invocation.json')
}

// Get Submission Info
if (options.submission) {
  return rating.getGetTransferInfo(config.submissionId)
}

if (options.compare) {
  return compare.compareDtos(config.dtoFiles[0], config.dtoFiles[1])
}

if (options.versions) {
  return version.getVersions()
}

if (options.testDTO) {
  return testDTO.testTransferDTO()
}

if (options.runTransferTest) {
  return transferDTO.transferDTOTest(options.runTransferTest)
}

if (options.runTransferHO) {
  return transferDTO.searchAndTransferHO()
}

if (options.runTransferAU) {
  return transferDTO.searchAndTransferAU()
}

if (options.getRatingIds) {
  return testDTO.getRecentRatings(options.getRatingIds)
}

if (options.submitGW) {
  return services.submitTransferPayload(options.submitGW)
}

if (options.ratingServiceProperty) {
  return services.callRatingServiceProperty(options.ratingServiceProperty)
}

if (options.ratingServiceAuto) {
  return services.callRatingServiceAuto(options.ratingServiceAuto)
}

if (options.productModel) {
  return pm.getAllProductModels()  
}

if (options.acord) {
  return acord.checkAcordValidity(options.acord)
}

if (options.manifestList) {
  return oe.getOEManifestNames(options.manifestList)
}

if (options.manifest) {
  return oe.getOEManifest(options.manifest)
}

if (options.recentInvcs) {
  return oe.getRecentInvocations(options.recentInvcs)
}

// If no option entered then show the help
console.log('Specify -h for help')
return

// Check input validity
function checkValidity(config) {
  // If output folder doesn't exist then create one.
  if (!fs.existsSync(config.outputFolder)) {
    console.info(`Output folder doesn't exist. Creating it : ${config.outputFolder}`)
    fs.mkdirSync(config.outputFolder)
  }

  // Check if Acord XML exist. 
  if (config.acordXmlFile && !fs.existsSync(config.acordXmlFile)) {
    console.error(`Acord file doesn't exist : ${config.acordXmlFile}`)
    return false
  }

  // If compare DTO option is given check the file exist
  if (config.dtoFiles) {
    config.dtoFiles.forEach(file => {
      if (!fs.existsSync(file)) {
        console.error(`File ${file} does not exist`.red)
        return false
      }
    })
  }

  return true
}