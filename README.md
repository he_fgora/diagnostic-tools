# README #

Diagnostic tool for able to issue rate and transfer call with the given Acord request and gather the all informtion like : 

As part of Rating request the tool retrieves the following into the files : 

* SAPI Answers. 
* OE Invocation in inquiry for domain service calls 
* OE Invocation in offer adapters for call to Rating and UW Service. 
    This will have teh DTO sent to Rating service. 
* Acord Response 

As part of the Transfer Request, the tool retrieves the followin into the files : 

* Response from Guidewire / Producer Engage. 
* Get policy submission information from PE. 
* TODO: Get the payload sent to GW. 

### How do I get set up? ###

* Setup node on you machine. 
* Run following setup command first to install the required depdencies : 
> npm install

* Initialize the secrets required to get Auth Token for CompRater
    * For now reach out to Feroz Gora to get this (.config) file. 
    * Copy this file to : *~/.e1p/.config*  (create the .e1p folder)
    * TODO: format of .config file.

### Using the Tool ###

* The tool is in the folder : cr-rating
> cd cr-rating

* Run the help command.
> node main.js -h

* Issue a Rating and Transfer request given an accord path 
> node main.js -r path/to/Acord.xml

* Look for the files created in the folder where Acord file exists. 
* You could use -o option to specify the output files need to be redicred to. e.g:

> node main.js -r path/to/Acord.xml -o ./output/files

* Look at the help to run other commands as well, like : 
    * Initiate a transfer given the System ID (internaally called OfferSet ID)
    * Get Answers given the Inquiry id. 
    * Get OE invocation information given the OE Invocation ID.

### Who do I talk to? ###

Feroz Gora (he_fgora@homesite.com)